## Simulation-Verification

Simulation-based formal verification of MCS, Anderson protocols.
- For Anderson protocol, please see the readme in `anderson` directory.
- For MCS protocol, please see the readme in `mcs` directory.
- `tas.cafe`: specification, proof scores of the simple mutual exclusion protocol - TAS.