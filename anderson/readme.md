### Simulation-based verification of Anderson protocol
- `snat.cafe`: defines SIMPLE-NAT, LABEL modules.
- `anderson.cafe`: original version of Anderson protocol.
- `a-anderson.cafe`: abstract version, A-Anderson.
- `proof_score` directory: proof scores of A-Anderson.
- `simulation` directory: 
  * `simulation.cafe`: define the simulation (the relation between two versions).
  * `proof_simulation.cafe`: proof of the simulation relation between two versions.
  * `ander2_implies_ander.cafe`: proof mutex of Anderson using simulation and mutex of A-Anderson.
  * `proof_sim1.cafe`, `proof_sim2.cafe`, `proof_sim3.cafe`: three parts of the proof simulation corresponding to exit, try, want, respectively (init is included in `proof_sim1.cafe` with exit). Three files are equivalent with `proof_simulation.cafe` file.
- `proof_inv0.cafe`: proof scores for inv0 of original version. Notice that, the proof of simulation requires to use the invariant inv0 of original version and three invariants inv9, inv10, inv11 of the abstract version.